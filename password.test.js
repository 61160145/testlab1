const {
  checkLength,
  checkAlphabet,
  checkSymbol,
  checkPassword
} = require('./password')
describe('Test Password Length', () => {
  test('should 8 characters to be true', () => {
    expect(checkLength('12345678')).toBe(true)
  })

  test('should 7 characters to be false', () => {
    expect(checkLength('1234567')).toBe(false)
  })

  test('should 25 characters to be true', () => {
    expect(checkLength('1234567891234567891234567')).toBe(true)
  })

  test('should 26 characters to be false', () => {
    expect(checkLength('12345678912345678912345678')).toBe(false)
  })
})

describe('Test Alphabet', () => {
  test('should has alphabet to password', () => {
    expect(checkAlphabet('m')).toBe(true)
  })
  test('should has alphabet A to password', () => {
    expect(checkAlphabet('A')).toBe(true)
  })
  test('should has not alphabet to password', () => {
    expect(checkAlphabet('5555')).toBe(false)
  })
})

describe('Test Symbol', () => {
  test('should has symbol * to password', () => {
    expect(checkSymbol('mmm*mm')).toBe(true)
  })
  test('should has symbol ! to password', () => {
    expect(checkSymbol('aaa!aa')).toBe(true)
  })
  test('should has not symbol to password', () => {
    expect(checkSymbol('11111')).toBe(false)
  })
})

describe('Test Password', () => {
  test('should password Boy@12 to be false', () => {
    expect(checkPassword('Boy@12')).toBe(false)
  })
})

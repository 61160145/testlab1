const checkLength = function (password) {
  return password.length >= 8 && password.length <= 25
}

const checkAlghabet = function (password) {
  const alphabets = 'abdcefghijklmnopqrstuvwxyz'
  for (const ch of password) {
    if (alphabets.includes(ch.toLowerCase())) return true
  }
  return false
  // return /[a-zA-Z]/.test(password)
}

const checkDigit = function (password) {
  return true
}

const checkSymbol = function (password) {
  const symbols = '!"#$%&()*+,-./:;<=>?@[]^_`{|}~'
  for (const ch of password) {
    if (symbols.includes(ch.toLowerCase())) return true
  }
  return false
}

const checkPassword = function (password) {
  return checkLength(password) &&
  checkAlghabet(password) &&
  checkDigit(password) &&
  checkSymbol(password)
}

module.exports = {
  checkLength,
  checkAlghabet,
  checkDigit,
  checkSymbol,
  checkPassword
}
